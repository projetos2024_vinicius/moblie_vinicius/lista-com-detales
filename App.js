import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListaDeTarefas from './src/taskList';
import TaskDetails from './src/taskDetails';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="ListaDeTarefas" component={ListaDeTarefas}/>
        <Stack.Screen 
        name="DetalhesDasTarefas"
         component={TaskDetails}
         options={{title:"Informações das Tarefas"}}
         />
      </Stack.Navigator>
    </NavigationContainer>
  );
};